#version 330 core

in vec2 vUV;

uniform sampler2D tex;

out vec4 color;

void main()
{
    vec4 frag_color = texture(tex, vUV);
   // float gray_color = (frag_color.r + frag_color.g + frag_color.b) / 3.0; // Take the average of the colors, there are better techniques but this is sufficient
   // color = vec4(gray_color, gray_color, gray_color, 1.0);

   vec3 sepia_color=vec3(frag_color);

   float red_sepia= dot(vec3(0.393,0.769,0.189),sepia_color);

   float green_sepia= dot(vec3(0.349,0.686,0.168),sepia_color);

   float blue_sepia= dot(vec3(0.272,0.534,0.131),sepia_color);

   color=vec4(red_sepia,green_sepia,blue_sepia,1.0);
}