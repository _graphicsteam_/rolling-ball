#include <application.hpp>
#include "Rolling_Ball\RollingBallScene.h"
#include "win\win.hpp"
#include "lose\lose.hpp"


int main()
{
	int state;
    Application* app = new Application("Rolling Ball", 640, 480, false);
    Scene* scene = new RollingBallScene(app);
	app->setScene(scene);
	state =  app->run();
	
	if (state == 1)
	{
		app = new Application("Congratulations !!", 640, 480, false);
		scene = new WinScene(app);
		app->setScene(scene);
		app->run();
	}
	else if (state == 2) {
		app = new Application("Nice Try!!", 640, 480,false);
		scene = new LoseScene(app);
		app->setScene(scene);
		state = app->run();
	}
    delete scene;
    delete app;

    return 0;
}
