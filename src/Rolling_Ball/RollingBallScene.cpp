#include "../Rolling_Ball/RollingBallScene.h"
#include "../../common/application.hpp"
#include "../../common/mesh/mesh_utils.hpp"
#include "../../common/textures/texture_utils.hpp"
#include "../../vendor/glm/glm/gtc/constants.hpp"
#include "../../vendor/glm/glm/trigonometric.hpp"
#include "../../vendor/glm/glm/gtc/matrix_transform.hpp"
#include "../../vendor/glm/glm/gtc/type_ptr.hpp"



const int WIDTH = 400;
const int HEIGHT = 600;

void RollingBallScene::Initialize()
{
	shader = new Shader();
	shader->attach("assets/shaders/directional.vert", GL_VERTEX_SHADER);
	shader->attach("assets/shaders/directional.frag", GL_FRAGMENT_SHADER);
	shader->link();

	skyShader = new Shader();
	skyShader->attach("assets/shaders/sky.vert", GL_VERTEX_SHADER);
	skyShader->attach("assets/shaders/sky.frag", GL_FRAGMENT_SHADER);
	skyShader->link();

	ground = MeshUtils::Plane({ 0,0 }, { 5,5 });
	sky = MeshUtils::Box();
	model = MeshUtils::LoadObj("assets/models/goal/goal.obj");
	Ball = MeshUtils::Sphere();

	balls1[0] = glm::vec3(-50, 3, 10);
	balls1[1] = glm::vec3(-75, 3, 10);
	balls1[2] = glm::vec3(-100, 3, 10);
	balls1[3] = glm::vec3(-125, 3, 10);
	balls2[0] = glm::vec3(-50, 3, 30);
	balls2[1] = glm::vec3(-70, 3, 30);
	balls2[2] = glm::vec3(-90, 3, 30);
	balls2[3] = glm::vec3(-105, 3, 30);
	balls3[0] = glm::vec3(-60, 3, -10);
	balls3[1] = glm::vec3(-80, 3, -10);
	balls3[2] = glm::vec3(-105, 3, -10);
	balls3[3] = glm::vec3(-120, 3, -10);
	balls4[0] = glm::vec3(-20, 3, -30);
	balls4[1] = glm::vec3(-70, 3, -30);
	balls4[2] = glm::vec3(-120, 3, -30);
	balls4[3] = glm::vec3(-150, 3, -30);
	balls4[4]= glm::vec3(-190, 3, -30);
	ballposition = glm::vec3(0, 3, 40);


	ball[ALBEDO] = TextureUtils::Load2DTextureFromFile("assets/textures/type4.png");
	ball[SPECULAR] = TextureUtils::Load2DTextureFromFile("assets/textures/Metal_spc.jpg");
	ball[ROUGHNESS] = TextureUtils::Load2DTextureFromFile("assets/textures/Metal_rgh.jpg");
	ball[AMBIENT_OCCLUSION] = TextureUtils::Load2DTextureFromFile("assets/textures/Suzanne_ao.jpg");
	ball[EMISSIVE] = TextureUtils::SingleColor({ 0.305,0,0.0305,1 });


	lastball[ALBEDO] = TextureUtils::Load2DTextureFromFile("assets/textures/type1.jpg");
	lastball[SPECULAR] = TextureUtils::Load2DTextureFromFile("assets/textures/type2.jpg");
	lastball[ROUGHNESS] = TextureUtils::Load2DTextureFromFile("assets/textures/type3.jpg");
	lastball[AMBIENT_OCCLUSION] = TextureUtils::Load2DTextureFromFile("assets/textures/Suzanne_ao.jpg");
	lastball[EMISSIVE] = TextureUtils::Load2DTextureFromFile("assets/textures/Asphalt_em.jpg");


	metal_ball[ALBEDO] = TextureUtils::Load2DTextureFromFile("assets/textures/metalalbedo.jpg");
	metal_ball[SPECULAR] = TextureUtils::Load2DTextureFromFile("assets/textures/metalspecualr.jpg");
	metal_ball[ROUGHNESS] = TextureUtils::Load2DTextureFromFile("assets/textures/Metal_rgh.jpg");
	metal_ball[AMBIENT_OCCLUSION] = TextureUtils::Load2DTextureFromFile("assets/textures/metalambient.jpg");
	metal_ball[EMISSIVE] = TextureUtils::SingleColor({ 0.01,0.01,0.1,1 });

	wood_ball[ALBEDO] = TextureUtils::Load2DTextureFromFile("assets/textures/Wood_col.jpg");
	wood_ball[SPECULAR] = TextureUtils::Load2DTextureFromFile("assets/textures/Wood_spc.jpg");
	wood_ball[ROUGHNESS] = TextureUtils::Load2DTextureFromFile("assets/textures/Wood_rgh.jpg");
	wood_ball[AMBIENT_OCCLUSION] = TextureUtils::Load2DTextureFromFile("assets/textures/Asphalt_em.jpg");
	wood_ball[EMISSIVE] = TextureUtils::SingleColor({ 0.1,0.01,0.01,1 });

	modelTex[ALBEDO] = TextureUtils::Load2DTextureFromFile("assets/textures/Wood_col.jpg");
	modelTex[SPECULAR] = TextureUtils::Load2DTextureFromFile("assets/textures/Wood_spc.jpg");
	modelTex[ROUGHNESS] = TextureUtils::Load2DTextureFromFile("assets/textures/Wood_rgh.jpg");
	modelTex[AMBIENT_OCCLUSION] = TextureUtils::Load2DTextureFromFile("assets/textures/Asphalt_em.jpg");
	modelTex[EMISSIVE] = TextureUtils::SingleColor({ 0.1,0.01,0.01,1 });

	asphalt[ALBEDO] = TextureUtils::Load2DTextureFromFile("assets/textures/Asphalt_col.jpg");
	asphalt[SPECULAR] = TextureUtils::Load2DTextureFromFile("assets/textures/Asphalt_spc.jpg");
	asphalt[ROUGHNESS] = TextureUtils::Load2DTextureFromFile("assets/textures/Asphalt_rgh.jpg");
	asphalt[AMBIENT_OCCLUSION] = TextureUtils::Load2DTextureFromFile("assets/textures/Suzanne_ao.jpg");
	asphalt[EMISSIVE] = TextureUtils::Load2DTextureFromFile("assets/textures/Asphalt_em.jpg");

	
	groundTex[ALBEDO] = TextureUtils::Load2DTextureFromFile("assets/textures/Groundtexturealbedo.jpg");
	groundTex[SPECULAR] = TextureUtils::Load2DTextureFromFile("assets/textures/Groundtexturespecular.jpg");
	groundTex[ROUGHNESS] = TextureUtils::Load2DTextureFromFile("assets/textures/Groundtextureroughness.jpg");
	groundTex[AMBIENT_OCCLUSION] = TextureUtils::SingleColor({ 0.2,0.2,0.2,1 });
	groundTex[EMISSIVE] = TextureUtils::SingleColor({ 0,0,0,1 });


	camera = new Camera();
	glm::ivec2 windowSize = getApplication()->getWindowSize();
	camera->setupPerspective(glm::pi<float>() / 2, (float)windowSize.x / windowSize.y, 0.1f, 1000.0f);
	camera->setUp({ 0, 1, 0 });

	controller = new ThirdPersonCameraController(this, camera);
	//controller->setYaw(-glm::half_pi<float>());
	//controller->setPitch(-glm::quarter_pi<float>());
	//controller->setPosition({ 0, 10, 50 });

	sunYaw = sunPitch = glm::quarter_pi<float>();

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glClearColor(0.88f, 0.68f, 0.15f, 0.0f);
}
int RollingBallScene::status() {
	return state;
}
void RollingBallScene::Update(double delta_time)
{
	controller->update(delta_time);
	Keyboard* kb = getKeyboard();

	float pitch_speed = 1.0f, yaw_speed = 1.0f;

/*	sunPitch += (float)delta_time * pitch_speed;
	sunPitch -= (float)delta_time * pitch_speed;
	sunYaw += (float)delta_time * yaw_speed;
	sunYaw -= (float)delta_time * yaw_speed;
	*/


	if (kb->isPressed(GLFW_KEY_I)) sunPitch += (float)delta_time * pitch_speed;
	if (kb->isPressed(GLFW_KEY_K)) sunPitch -= (float)delta_time * pitch_speed;
	if (kb->isPressed(GLFW_KEY_L)) sunYaw += (float)delta_time * yaw_speed;
	if (kb->isPressed(GLFW_KEY_J)) sunYaw -= (float)delta_time * yaw_speed;

	if (sunPitch < -glm::half_pi<float>()) sunPitch = -glm::half_pi<float>();
	if (sunPitch > glm::half_pi<float>()) sunPitch = glm::half_pi<float>();
	sunYaw = glm::wrapAngle(sunYaw);
	
	int speed = 9;
	if (kb->isPressed(GLFW_KEY_UP)) 
	{ 
		ballposition.z -= (float)delta_time*speed*cos(controller->getYaw());
		ballposition.x += (float)delta_time*speed*sin(controller->getYaw());
		var = true;
	}
	if (kb->isPressed(GLFW_KEY_DOWN)) 
	{
		ballposition.z += (float)delta_time*speed*cos(controller->getYaw());
		ballposition.x -= (float)delta_time*speed*sin(controller->getYaw());
		var = true; 
	}
	if (kb->isPressed(GLFW_KEY_LEFT)) 
	{ 
		ballposition.z -= (float)delta_time*speed*sin(controller->getYaw());
		ballposition.x -= (float)delta_time*speed*cos(controller->getYaw());
		var = false;
	}
	if (kb->isPressed(GLFW_KEY_RIGHT)) {
		ballposition.x += (float)delta_time*speed*cos(controller->getYaw());
		ballposition.z += (float)delta_time*speed*sin(controller->getYaw());
		var = false;
	}

	int speed2 = 15;
	for (int i = 0; i < 4; i++)
	{
		balls1[i].x += (float)delta_time*speed2;
		if (balls1[i].x >= 50)
			balls1[i].x = -50;
		ballsrotation.x = glm::wrapAngle(balls1[i].x);
	}
	for (int i = 0; i < 4; i++)
	{
		balls2[i].x += (float)delta_time*speed2;
		if (balls2[i].x >= 50)
			balls2[i].x = -50;
		ballsrotation.x = glm::wrapAngle(balls2[i].x);
	}
	for (int i = 0; i < 4; i++)
	{
		balls3[i].x += (float)delta_time*speed2;
		if (balls3[i].x >= 50)
			balls3[i].x = -50;
		ballsrotation.x = glm::wrapAngle(balls3[i].x);
	}
	for (int i = 0; i < 5; i++)
	{
		balls4[i].x += (float)delta_time*speed2;
		if (balls4[i].x >= 50)
			balls4[i].x = -50;
		ballsrotation.x = glm::wrapAngle(balls3[i].x);
	}
	CheckCollision();


	//if (kb->isPressed(GLFW_KEY_Q)) ballrotation -= (float)delta_time;
	//if (kb->isPressed(GLFW_KEY_E)) ballrotation += (float)delta_time;
	ballrotation.x = glm::wrapAngle(ballposition.x);
	ballrotation.z = glm::wrapAngle(-ballposition.z);
}

inline glm::vec3 getTimeOfDayMix(float sunPitch) {
	sunPitch /= glm::half_pi<float>();
	if (sunPitch > 0) {
		float noon = glm::smoothstep(0.0f, 0.5f, sunPitch);
		return { noon, 1.0f - noon, 0 };
	}
	else {
		float dusk = glm::smoothstep(0.0f, 0.25f, -sunPitch);
		return { 0, 1.0f - dusk, dusk };
	}
}

void RollingBallScene::DrawBall()
{
	glm::mat4 ball_mat =glm::scale(glm::mat4(), glm::vec3(3, 3, 3)) ;
	if (var)
		ball_mat = glm::translate(glm::mat4(), { ballposition.x, 3, ballposition.z }) *glm::rotate(glm::mat4(), -ballrotation.z, { 1, 0, 0 })*ball_mat;
	else
		ball_mat = glm::translate(glm::mat4(), { ballposition.x, 3, ballposition.z })* glm::rotate(glm::mat4(), -ballrotation.x, { 0, 0, 1 })*ball_mat;
	shader->set("M", ball_mat);
	shader->set("M_it", glm::transpose(glm::inverse(ball_mat)));
	for (int i = 0; i < 5; i++) {
		glActiveTexture(GL_TEXTURE0 + i);
		ball[i]->bind();
	}
	float emissive_power = glm::sin((float)glfwGetTime()) + 1;
	shader->set("material.emissive_tint", glm::vec3(1, 1, 1) * emissive_power);
	Ball->draw();
}

void RollingBallScene::CheckCollision()
{
	float dist;
	for (int i = 0; i < 4; i++)
	{
		dist = glm::distance(balls1[i], ballposition);
		if (dist <= 6)
		{
			state = 2;
		}
	}
	for (int i = 0; i < 4; i++)
	{
		dist = glm::distance(balls2[i], ballposition);
			if (dist <= 6)
			{
				state = 2;
			}
	}
	for (int i = 0; i < 4; i++)
	{
		dist = glm::distance(balls3[i], ballposition);
		if (dist <= 6)
		{
			state = 2;
		}
	}
	for (int i = 0; i < 5; i++)
	{
		dist = glm::distance(balls4[i], ballposition);
		if (dist <= 6)
		{
			state = 2;
			
		}
	}
	if(ballposition.x>=50 || ballposition.x <= -50 || ballposition.z >= 50 || ballposition.x <= -50)
	{
		state = 2;
		
	}
	if(ballposition.z>-50 && ballposition.z<-45 && ballposition.x>-5 && ballposition.x < 5)
	{
		state = 1;
		
	}

}

void RollingBallScene::DrawBalls()
{
	for (int j = 0; j < 4; j++)
	{
		if (balls1[j].x > -50 && balls1[j].x < 50)
		{
			glm::mat4 ball_mat = glm::scale(glm::mat4(), glm::vec3(3, 3, 3));

			ball_mat = glm::translate(glm::mat4(), { balls1[j] })* glm::rotate(glm::mat4(), -ballsrotation.x, { 0, 0, 1 })*ball_mat;
			shader->set("M", ball_mat);
			shader->set("M_it", glm::transpose(glm::inverse(ball_mat)));
			for (int i = 0; i < 5; i++) {
				glActiveTexture(GL_TEXTURE0 + i);
				metal_ball[i]->bind();
			}
			float emissive_power = glm::sin((float)glfwGetTime()) + 1;
			shader->set("material.emissive_tint", glm::vec3(1, 1, 1) * emissive_power);
			Ball->draw();
		}
		for (int j = 0; j < 4; j++)
		{
			if (balls2[j].x > -50 && balls2[j].x < 50)
			{
				glm::mat4 ball_mat = glm::scale(glm::mat4(), glm::vec3(3, 3, 3));

				ball_mat = glm::translate(glm::mat4(), { balls2[j] })* glm::rotate(glm::mat4(), -ballsrotation.x, { 0, 0, 1 })*ball_mat;
				shader->set("M", ball_mat);
				shader->set("M_it", glm::transpose(glm::inverse(ball_mat)));
				for (int i = 0; i < 5; i++) {
					glActiveTexture(GL_TEXTURE0 + i);
					wood_ball[i]->bind();
				}
				float emissive_power = glm::sin((float)glfwGetTime()) + 1;
				shader->set("material.emissive_tint", glm::vec3(1, 1, 1) * emissive_power);
				Ball->draw();
			}
		}
		for (int j = 0; j < 4; j++)
		{
			if (balls3[j].x > -50 && balls3[j].x < 50)
			{
				glm::mat4 ball_mat = glm::scale(glm::mat4(), glm::vec3(3, 3, 3));

				ball_mat = glm::translate(glm::mat4(), { balls3[j] })* glm::rotate(glm::mat4(), -ballsrotation.x, { 0, 0, 1 })*ball_mat;
				shader->set("M", ball_mat);
				shader->set("M_it", glm::transpose(glm::inverse(ball_mat)));
				for (int i = 0; i < 5; i++) {
					glActiveTexture(GL_TEXTURE0 + i);
					asphalt[i]->bind();
				}
				float emissive_power = glm::sin((float)glfwGetTime()) + 1;
				shader->set("material.emissive_tint", glm::vec3(1, 1, 1) * emissive_power);
				Ball->draw();
			}
		}
		for (int j = 0; j < 5; j++)
		{
			if (balls4[j].x > -50 && balls4[j].x < 50)
			{
				glm::mat4 ball_mat = glm::scale(glm::mat4(), glm::vec3(3, 3, 3));

				ball_mat = glm::translate(glm::mat4(), { balls4[j] })* glm::rotate(glm::mat4(), -ballsrotation.x, { 0, 0, 1 })*ball_mat;
				shader->set("M", ball_mat);
				shader->set("M_it", glm::transpose(glm::inverse(ball_mat)));
				for (int i = 0; i < 5; i++) {
					glActiveTexture(GL_TEXTURE0 + i);
					lastball[i]->bind();
				}
				float emissive_power = glm::sin((float)glfwGetTime()) + 1;
				shader->set("material.emissive_tint", glm::vec3(1, 1, 1) * emissive_power);
				Ball->draw();
			}
		}
	}

}

void RollingBallScene::Draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //Clear colors and depth

	glm::mat4 VP = camera->getVPMatrix();
	glm::mat4 viewMatrix = camera->getViewMatrix();
	glm::vec3 cam_pos = camera->getPosition();
	glm::vec3 sun_direction = glm::vec3(glm::cos(sunYaw), 0, glm::sin(sunYaw)) * glm::cos(sunPitch) + glm::vec3(0, glm::sin(sunPitch), 0);

	const glm::vec3 noonSkyColor = { 0.53f, 0.81f, 0.98f };
	const glm::vec3 sunsetSkyColor = { 0.99f, 0.37f, 0.33f };
	const glm::vec3 duskSkyColor = { 0.04f, 0.05f, 0.19f };

	const glm::vec3 noonSunColor = { 0.9f, 0.8f, 0.6f };
	const glm::vec3 sunsetSunColor = { 0.8f, 0.6f, 0.4f };
	const glm::vec3 duskSunColor = { 0.0f, 0.0f, 0.0f };

	glm::vec3 mix = getTimeOfDayMix(sunPitch);

	glm::vec3 skyColor = mix.x * noonSkyColor + mix.y * sunsetSkyColor + mix.z * duskSkyColor;
	glm::vec3 sunColor = mix.x * noonSunColor + mix.y * sunsetSunColor + mix.z * duskSunColor;

	shader->use();
	shader->set("VP", VP);
	shader->set("viewMatrix", viewMatrix);
	shader->set("cam_pos", cam_pos);
	shader->set("light.color", sunColor);
	shader->set("light.direction", -sun_direction);
	shader->set("ambient", 0.5f*skyColor);
	shader->set("skyColour", skyColor);
	shader->set("material.albedo", 0);
	shader->set("material.specular", 1);
	shader->set("material.roughness", 2);
	shader->set("material.ambient_occlusion", 3);
	shader->set("material.emissive", 4);

	shader->set("material.albedo_tint", { 1,1,1 });
	shader->set("material.specular_tint", { 1,1,1 });
	shader->set("material.roughness_scale", 1.0f);
	shader->set("material.emissive_tint", { 1,1,1 });

	glm::mat4 ground_mat = glm::scale(glm::mat4(), glm::vec3(50, 1, 50));
	shader->set("M", ground_mat);
	shader->set("M_it", glm::transpose(glm::inverse(ground_mat)));
	for (int i = 0; i < 5; i++) {
		glActiveTexture(GL_TEXTURE0 + i);
		groundTex[i]->bind();
	}
	ground->draw();

	DrawBall();
	DrawBalls();
	glm::mat4 model2_mat = glm::scale(glm::mat4(), glm::vec3(5, 5, 5))*glm::translate(glm::mat4(), { 0, 0, -9 });
	shader->set("M", model2_mat);
	shader->set("M_it", glm::transpose(glm::inverse(model2_mat)));
	for (int i = 0; i < 5; i++) {
		glActiveTexture(GL_TEXTURE0 + i);
		modelTex[i]->bind();
	}
	model->draw();
	//Draw SkyBox
	skyShader->use();
	skyShader->set("VP", VP);
	skyShader->set("cam_pos", cam_pos);
	skyShader->set("M", glm::translate(glm::mat4(), cam_pos));
	skyShader->set("sun_direction", sun_direction);
	skyShader->set("sun_size", 0.02f);
	skyShader->set("sun_halo_size", 0.02f);
	skyShader->set("sun_brightness", 1.0f);
	skyShader->set("sun_color", sunColor);
	skyShader->set("sky_top_color", skyColor);
	skyShader->set("sky_bottom_color", 1.0f - 0.25f*(1.0f - skyColor));
	skyShader->set("sky_smoothness", 0.5f);


	glCullFace(GL_FRONT);
	sky->draw();
	glCullFace(GL_BACK);
}

void RollingBallScene::Finalize()
{
	delete controller;
	delete camera;
	delete model;
	delete sky;
	delete ground;
	delete Ball;
	for (int i = 0; i < 5; i++) {
		delete groundTex[i];
		delete wood_ball[i];
		delete metal_ball[i];
		delete ball[i];
		delete lastball[i];
		delete modelTex[i];
		delete asphalt[i];
	}
	delete skyShader;
	delete shader;
}	