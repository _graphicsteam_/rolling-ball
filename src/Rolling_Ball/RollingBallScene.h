#pragma once
#define GLM_FORCE_CXX11



#include "../../common/scene.hpp"
#include "../../common/shader.hpp"
#include "../../common/mesh/mesh.hpp"
#include "../../common/textures/texture2d.hpp"
#include "../../common/framebuffer.hpp"
#include "../../common/camera/camera.hpp"
#include "../../common/camera/controllers/fly_camera_controller.hpp"
#include "../../common/camera/controllers/3rd_person_camera_controller.hpp"
#include <application.hpp>


enum TextureType {
	ALBEDO = 0,
	SPECULAR = 1,
	ROUGHNESS = 2,
	AMBIENT_OCCLUSION = 3,
	EMISSIVE = 4
};


class RollingBallScene : public Scene {
private:

	bool frameVPInitialized = false;
	Shader* shader;
	Shader* skyShader;
	Mesh* ground;
	Mesh* sky;
	Mesh* model;
	Mesh* Ball;
	Texture2D* ball[5];
	Texture2D* metal_ball[5];
	Texture2D* wood_ball[5];

	Texture2D* wood[5];
	Texture2D* asphalt[5];
	Texture2D* modelTex[5];
	Texture2D* groundTex[5];
	Texture2D* skyTex[5];
	Texture2D* lastball[5];
	Camera* camera;
	ThirdPersonCameraController* controller;
	GLuint mLoc, vpLoc;
	bool var=true;
	int state=0;
	glm::vec3 balls1[4];
	glm::vec3 balls2[4];
	glm::vec3 balls3[4];
	glm::vec3 balls4[5];

	glm::vec3 ballsrotation;
	//Scene*win, lose;
	//Application*winApp, loseApp;

	float sunYaw, sunPitch;
	void DrawBall();
	void DrawBalls();
	void CheckCollision();
	
public:
	RollingBallScene(Application* app) : Scene(app) {}
	int status() override;
	void Initialize() override;
	void Update(double delta_time) override;
	void Draw() override;

	void Finalize() override;
};
