#include "win.hpp"

#include "../../common/application.hpp"
#include "../../common/mesh/mesh_utils.hpp"
#include "../../common/textures/texture_utils.hpp"
#include "../../vendor/glm/glm/gtc/constants.hpp"
#include "../../vendor/glm/glm/trigonometric.hpp"
#include "../../vendor/glm/glm/gtc/matrix_transform.hpp"
#include "../../vendor/glm/glm/gtc/type_ptr.hpp"

void WinScene::Initialize() {
	shader = new Shader();
	shader->attach("assets/shaders/texture.vert", GL_VERTEX_SHADER);
	shader->attach("assets/shaders/texture.frag", GL_FRAGMENT_SHADER);
	shader->link();

	mvpLoc = glGetUniformLocation(shader->getID(), "MVP");
	texLoc = glGetUniformLocation(shader->getID(), "tex");

	mesh = MeshUtils::Plane({ 0,0 }, { 1,1 });
	//mesh = MeshUtils::Sphere(4, 4);
	glm::vec4 purple = { 1, 0, 1, 1 };

	tex1 = TextureUtils::Load2DTextureFromFile("assets/textures/win2.png");
	tex1->bind();
	//Wrap parameter define what to do if the UVs are less than 0 or greater than 1
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//Since a screen pixel can see more than one texel (Minification) or vice versa (Magnification), the filters are used
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//Define Border color if needed (used by Wrap-mode GL_CLAMP_TO_BORDER)
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, (const GLfloat*)&purple);

	camera = new Camera();
	glm::ivec2 windowSize = getApplication()->getWindowSize();
	camera->setupPerspective(glm::half_pi<float>(), (float)windowSize.x / windowSize.y, 0.01f, 100.0f);
	cam_position = { 0, 1, 0 };
	camera->setPosition(cam_position);
	camera->setDirection({ 0, -1, 0 });
	camera->setUp({ 0, 0, -1 });

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

void WinScene::Update(double delta_time) {

	}

void WinScene::Draw() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //Clear colors and depth

	camera->setPosition(cam_position);
	glm::mat4 VP = camera->getVPMatrix();

	shader->use();

	//Activate Texture unit 0
	glActiveTexture(GL_TEXTURE0);
	//Bind texture (Now it is bound to unit 0)
	tex1->bind();
	//Send 0 to the sampler so that it samples from unit 0
	glUniform1i(texLoc, 0);
	glUniformMatrix4fv(mvpLoc, 1, GL_FALSE, glm::value_ptr(VP * glm::scale(glm::mat4(), { 2,1,1 })));

	mesh->draw();
}

void WinScene::Finalize() {
	delete camera;
	delete tex1;
	delete mesh;
	delete shader;
}