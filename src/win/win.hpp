#define GLM_FORCE_CXX11


#include "../../vendor/glm/glm/glm.hpp"

#include "../../common/scene.hpp"
#include "../../common/shader.hpp"
#include "../../common/mesh/mesh.hpp"
#include "../../common/textures/texture2d.hpp"
#include "../../common/framebuffer.hpp"
#include "../../common/camera/camera.hpp"


class WinScene : public Scene {
private:
	Shader* shader;
	Mesh* mesh;
	Camera* camera;
	Texture2D *tex1, *tex2;

	glm::vec3 cam_position;

	GLuint mvpLoc, texLoc;
public:
	WinScene(Application* app) : Scene(app) {}

	void Initialize() override;
	void Update(double delta_time) override;
	void Draw() override;
	void Finalize() override;
};
